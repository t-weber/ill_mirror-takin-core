#!/bin/bash
#
# invokes "themakefile" (alternate build path)
# @author Tobias Weber <tobias.weber@tum.de>
# @license GPLv2
#

MAKE=make
GCC=$(which gcc 2>/dev/null)
SKIP_PREBUILD=0


if [ "$GCC" != "" ]	# don't check version if only e.g. clang is available
then
	GCC_VER=$($GCC -dumpversion)
	echo -e "GCC is version $GCC_VER"

	GCC_VER=(${GCC_VER//./ })
fi




if [ ! -d tlibs ] || [ ! -f tlibs/AUTHORS ]
then
	echo -e "Error: tlibs not installed."
	exit -1;
fi




if [ "$SKIP_PREBUILD" == "0" ]
then
	echo -e "Prebuilding..."
	if ! ./prebuild.sh
	then
		echo -e "Error: Prebuild failed";
		exit -1;
	fi
fi




NPROC=$(which nproc 2>/dev/null)
if [ "$NPROC" == "" ]; then NPROC="/usr/bin/nproc"; fi


if [ ! -x $NPROC ]
then
	CPUCNT=2
else
	CPUCNT=$($NPROC --ignore=1)
fi


echo -e "\nBuilding using $CPUCNT processes..."
${MAKE} -j${CPUCNT} -f themakefile
